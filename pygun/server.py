from flask import Flask, request, send_from_directory, send_file, render_template, jsonify
from flask_sockets import Sockets
from time import sleep
import json
import os
from .utils import *
import redis
import time 
import uuid

dir_path = os.path.dirname(os.path.realpath(__file__))


app = Flask(__name__)
sockets = Sockets(app)


@app.route('/static/<path:path>')
def send_public(path):
    return send_from_directory('static' + '/' + path)

peers = []
graph = {} 
trackedids = []

def trackid(id_):
    if id_ not in trackedids:
        trackedids.append(id_)
    return id_


def emit(data):
    resp = json.dumps(data)
    for p in peers:
        print("Sending resp: ", resp, " to ", p)
        p.send(resp)


@sockets.route('/gun')
def gun(ws):

    global peers, graph
    print("GRAPH is :", graph)
    print("Got connection: ", ws)
    peers.append(ws)
    print("Peers are: ", peers)

    while not ws.closed:
        msgstr = ws.receive()
        if msgstr is not None:
            msg = json.loads(msgstr)
            if not isinstance(msg, list):
                msg = [msg]
            for payload in msg:
                print("\nProcessing payload: ", payload, type(payload))
                if isinstance(payload, str):
                    payload = json.loads(payload)
                if 'put' in payload:
                    change = payload['put']
                    soul = payload['#']
                    
                    diff = ham_mix(change, graph)
                    uid = str(uuid.uuid4())
                    trackid(uid)
                    print(" in put request\n\n")
                    print("after ham mix: ", diff)
                    print("\n\nGraph now is:  ", graph)
                    resp = {'@':soul, '#':uid, 'ok':True}
                elif 'get' in payload:
                    get = payload['get']
                    soul = get['#']
                    ack = lex_from_graph(get, graph)
                    uid = str(uuid.uuid4())
                    trackid(uid)
                    resp = {'put': ack, '@':soul, '#':uid, 'ok':True}
                    print("\n\nin a get request and sending: ", resp, "\n\n")

            emit(resp)
            emit(msg)


    print("Connection closed for ws: ", ws)
    peers.remove(ws)
    print("Peers now are: ", peers)

